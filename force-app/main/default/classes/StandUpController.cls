public with sharing class StandUpController {
    @AuraEnabled(cacheable=false)
    public static Stand_Up__c getRandomResponse(String responseType) {
        List<Stand_Up__c> responseList;

        if (responseType.equals('all')) {
            responseList = [SELECT Name, Type__c FROM Stand_Up__c];
        } else {
            responseList = [SELECT Name, Type__c FROM Stand_Up__c WHERE Type__c = :responseType];
        }

        return getRandomFromList(responseList);
    }

    private static Stand_Up__c getRandomFromList(List<Stand_Up__c> responseList) {
        if (responseList == null || responseList.isEmpty()) {
            Stand_Up__c standUpResponse = new Stand_Up__c();
            standUpResponse.Name = 'Ummmm.......';
            standUpResponse.Type__c = 'Neutral';

            return standUpResponse;
        }

        Integer count = responseList.size();
        Integer rand = Math.floor(Math.random() * count).intValue();

        return responseList[rand];
    }
}
