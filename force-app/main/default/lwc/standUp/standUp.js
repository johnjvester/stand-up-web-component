import { LightningElement } from 'lwc';
import getRandomResponse from '@salesforce/apex/StandUpController.getRandomResponse';

export default class StandUp extends LightningElement {
    standUpResponse = null;
    error = null;

    getPositiveResponse() {
        this.getRandomResponseFromSalesforce('Positive');
    }

    getNeutralResponse() {
        this.getRandomResponseFromSalesforce('Neutral');
    }

    getNegativeResponse() {
        this.getRandomResponseFromSalesforce('Negative');
    }

    getAnyResponse() {
        this.getRandomResponseFromSalesforce('all');
    }

    getRandomResponseFromSalesforce(responseType) {
        this.standUpResponse = null;
        this.error = null;
        getRandomResponse({responseType})
        .then((standUp) => {
            this.standUpResponse = standUp.Name;
        })
        .catch((error) => {
            this.error = JSON.stringify(error);
        });
    }
}